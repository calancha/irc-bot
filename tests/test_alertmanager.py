"""Alertmanager modules test."""
import unittest

from irc_bot import alertmanager

EMOJI_SEVERITY = {
    'reminder': '⏰',
    'info': 'ℹ️',
    'warning': '⚠️',
    'important': '🔴',
    'serious': '🔥',
}


class TestAlertManager(unittest.TestCase):
    """ Test alertmanager."""

    def test_alert_emoji(self):
        """Test _alert_emoji module."""
        cases = [
            ({'status': 'resolved'}, '🔵'),
            ({'status': 'firing', 'labels': {'severity': 'reminder'}}, '⏰'),
            ({'status': 'firing', 'labels': {'severity': 'info'}}, 'ℹ️'),
            ({'status': 'firing', 'labels': {'severity': 'important'}}, '🔴'),
            ({'status': 'firing', 'labels': {'severity': None}}, '🔴'),
            ({'status': 'firing'}, '🔴'),
        ]
        for alert, expected in cases:
            self.assertEqual(
                alertmanager._alert_emoji(alert),  # pylint: disable=protected-access
                expected
            )
