"""Webhook interaction tests."""
import unittest
from unittest import mock

from irc_bot import webhook


class TestWebhook(unittest.TestCase):
    """ Test webhooks."""

    def setUp(self):
        """SetUp class."""
        self.client = webhook.app.test_client()

    def test_index(self):
        """Check the index page."""
        response = self.client.get('/')
        self.assertEqual(response.status, '200 OK')

    @mock.patch('irc_bot.webhook.IRC_PUBLISH_QUEUE')
    def test_message(self, queue):
        """Check routing of a message."""
        response = self.client.post(
            '/message', json={'message': 'content'})
        self.assertEqual(response.status, '200 OK')
        queue.put.assert_called_with('content')

    @mock.patch('irc_bot.webhook.IRC_PUBLISH_QUEUE')
    @mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
    def test_alertmanager(self, queue):
        """Check routing of a Grafana alert."""
        response = self.client.post(
            '/alertmanager', json={
                "externalURL": "https://externalurl",
                "alerts": [{
                    "status": "firing",
                    "labels": {"alertname": "name1"},
                    "annotations": {"summary": "summary1"},
                }, {
                    "status": "resolved",
                    "labels": {"alertname": "name2"},
                    "annotations": {"summary": "summary2",
                                    "dashboard": "https://dashboard2"},
                }]})
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(len(queue.put.mock_calls), 2)

        self.assertIn('RESOLVED', queue.put.mock_calls[0].args[0])
        self.assertIn('name2', queue.put.mock_calls[0].args[0])
        self.assertIn('summary2', queue.put.mock_calls[0].args[0])
        self.assertIn('dashboard2', queue.put.mock_calls[0].args[0])

        self.assertIn('externalurl', queue.put.mock_calls[1].args[0])
        self.assertIn('FIRING', queue.put.mock_calls[1].args[0])
        self.assertIn('name1', queue.put.mock_calls[1].args[0])
        self.assertIn('summary1', queue.put.mock_calls[1].args[0])
